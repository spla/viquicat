# viquiCAT
viquiCAT is a Wikipedia bot that reply queries to fediverse users from its related Mastodon's server account.  
Query the bot is this easy:  

`@wikipedia_bot_account consulta <fediverse_users_query>`  
  
### Dependencies

-   **Python 3**
-   Mastodon's bot account  

### Usage:

1. Clone this repo: `git clone https://gitlab.com/spla/viquicat.git <target dir>`  

2. cd into your `<target dir>` and create the Python Virtual Environment: `python3.x -m venv .`  
 
3. Activate the Python Virtual Environment: `source bin/activate`  
  
4. Run `pip install -r requirements.txt` to install needed libraries.  

5. Run `python setup.py` to setup the Mastodon bot account & access tokens.  

6. Use your favourite scheduling method to set `python viquicat.py` to run every minute.  

21.3.2022 **New feature** Ray Parallel processing! The code replies all queries at the same time!   

